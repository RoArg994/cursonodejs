let Bicicleta = function(id, color, modelo, ubicacion) {

    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;

}
// creamos un metodo para visualizar los atributos de un objeto Bicicleta
Bicicleta.prototype.toString = function() {
     return "Objeto Bicicleta";
}
// agregamos un atributo de arreglo vacio
Bicicleta.allBicis = [];
// creamos un metodo para agregar bicis en el arreglo allBicis
Bicicleta.add = bici => {
    Bicicleta.allBicis.push(bici);
}

// metodo para buscar bici
Bicicleta.findById = function(biciId){
    
    var aBici = Bicicleta.allBicis.find( x => x.id == biciId);

    if(aBici)
        return aBici;
    else
        throw new Error('No existe una bicicleta con ese id', biciId);

}

// metodo remove
Bicicleta.removeById = function(biciId){
    
    this.findById(biciId);
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if ( Bicicleta.allBicis[i].id == biciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
        
    }


}

let b1 = new Bicicleta(1, 'roja', 'urbana',[-28.461898, -65.788507]);
var b2 = new Bicicleta(2, 'Amarilla', 'Montaña', [-28.458197, -65.787170]);
var b3 = new Bicicleta(3, 'Negra', 'Playera', [-28.460099, -65.788177]);

Bicicleta.add(b1);
Bicicleta.add(b2);
Bicicleta.add(b3);


module.exports = Bicicleta;