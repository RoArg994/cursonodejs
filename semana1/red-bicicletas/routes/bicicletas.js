// ahora hay crear la ruta para renderizar nuestro template
// importamos el modelo de rutas de express
var express = require('express');
var router = express.Router();
// action del index es buena practica escribir el archivo asi
// bicicleta.controller.js
let bicicletaController = require('../controllers/bicicleta');
// rutas
router.get('/', bicicletaController.bicicleta_list);
router.get('/create', bicicletaController.bicicleta_create_get);
router.post('/create', bicicletaController.bicicleta_create_post);
router.get('/:id/update', bicicletaController.bicicleta_update_get);
router.post('/:id/update', bicicletaController.bicicleta_update_post);
router.post('/:id/delete', bicicletaController.bicicleta_delete_post);

module.exports = router;