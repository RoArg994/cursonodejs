//***** importaciones de modulos y frameworks *****/
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('./config/passport');
const Usuario = require('./models/usuario');
const Token = require('./models/token');
const jwt = require('jsonwebtoken');

//*****  creacion de la app *****/
var app = express();
var mongoose = require('mongoose');

//***** importaciones de controller *****/
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var authApiRouter = require('./routes/api/auth');

const Usuario = require('./models/usuario');
const Token= require('./models/token');
const { session } = require('./config/passport');

const store = new session.MemoryStore;
// autenticacion de token
app.set('secretKey', 'jwt_pwd_!!223344');

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret:'red_bicis_!!!***!".!".!".!".!".!".123123'
}));

//***** base de datos mongodb *****/
const mongoDB = 'mongodb://localhost/red_bicicletas';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));
db.once('open', () => {
  console.log("Conectado a "+ mongoDB);
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));


// ***** middleware ******/
function loggeIn(req, res, next){
  if (req.user){
    next();
  }else{
    console.log('Usuario no logueado');
    res.redirect('/login');
  }

}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if(err){
      res.json({status:"error", message: err.message, data: null});
    }else{
      req.body.userId = decoded.id;
      console.log('jwt verify: ' + decoded);

      next();
    }
  });
}

// ****** mis rutas *****/
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas',loggeIn, bicicletasRouter);
app.use('/usuarios', usuariosRouter);
// TOKEN
app.use('/token', tokenRouter);
// API
app.use('/api/auth', authApiRouter);
app.use('/api/bicicletas',validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);

app.get('/login', function(req, res){
  res.render('session/login');
});

app.post('/login', function(req, res, next){
  // passport
  passport.authenticate('local', function(err, usuario, info){
    if(err) return next(err);
    if(!usuario) return res.render('session/login', {info});
    req.logIn(usuario, function(err){
      if(err) return next(err);

      return res.redirect('/');
    });
  })(req, res, next); // aqui estamos llamando a la funcion despues de inicializarla y le pasamos tres argumentos
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

app.get('/forgotPassword', function(req, res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function(req, res){
  Usuario.findOne({ email: req.body.email }, function(err, usuario){
    if(!usuario) return res.render('session/forgotPassword', {info: {message: "No existe un usuario con ese email."}});
  });
  Usuario.resetPassword(function(err){
    if(err) return next(err);
    console.log('session/forgotPasswordMessage');
  });

  res.render('session/forgotPasswordMessage');
});

app.get('/resetPassword/:token', function(req, res, next){
  Token.findOne({ token: req.params.token }, function(err, token){
    if(!token) return res.status(400).send({ type: 'not-verified', msg: 'No existe un usuario asociado al token.'});
  });

  Usuario.findById(token._userId, function(err, usuario){
    if(!usuario) return res.status(400).send({msg: 'No existe un usuario asociado al token./Usuario'});
    res.render('session/resetPassword', {errors:{}, usuario: usuario});
  });
});

app.post('/resetPassword', function(req, res){
  if (req.body.password != req.body.confirm_password){
    res.render('session/resetPassword', {errors: {confirm_password: {message: 'No coinciden con el password ingresado'} }, usuario: new Usuario({email: req.body.email})});
    return;
  }
  Usuario.findOne({ email: req.body.email }, function(err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function(err){
      if (err){
        res.render('session/restPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})});
      }else{
        res.redirect('/login');
      }
    });
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




module.exports = app;
