const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');

passport.use(new LocalStrategy(
    function(email, password, done){
        Usuario.findOne({email: email}, function(err, usuario){
            if(err) return done(err);
            if(!usuario) return done(null, false, {message: 'No existe una cuenta con esa Email'});
            if(!usuario.validPassword(password)) return done(null, false, {message: 'Password incorrecto.'});
            
            return done(null, usuario);
        });
    }
));

passport.serializeUser(function(user, done) {
    done(null, user);
  });
  

  passport.deserializeUser(function(id ,done){
    Usuario.findById(id, function(err, usuario){
        cb(err, usuario);
    });
});
/***
 * passport.deserializeUser(function(user, done) {
    done(null, user);
  });
});**/

module.exports = passport;